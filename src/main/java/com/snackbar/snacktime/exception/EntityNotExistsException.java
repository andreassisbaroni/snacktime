package com.snackbar.snacktime.exception;

/**
 *
 * @author andre
 */
public class EntityNotExistsException extends Exception {

    public EntityNotExistsException(String message) {
        super(message);
    }
}
