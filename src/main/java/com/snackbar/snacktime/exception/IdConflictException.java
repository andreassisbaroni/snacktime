package com.snackbar.snacktime.exception;

/**
 *
 * @author andre
 */
public class IdConflictException extends Exception {

    public IdConflictException(String message) {
        super(message);
    }
}
