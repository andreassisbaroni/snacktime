package com.snackbar.snacktime.exception;

/**
 *
 * @author andre
 */
public class IdExistsException extends Exception {

    public IdExistsException(String message) {
        super(message);
    }
}
