package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.CartItem;
import com.snackbar.snacktime.domain.model.CartItemIngredient;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface to {@link CartItemIngredient} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface CartItemIngredientRepository extends JpaRepository<CartItemIngredient, Long> {

    /**
     * Finds a {@link Collection} of {@link CartItemIngredient} by it's your
     * {@link CartItem}, ordering by your id crescent.
     *
     * @param cartItem {@link CartItem} entity.
     * @return a {@link Collection} of {@link CartItemIngredient} entities.
     */
    Collection<CartItemIngredient> findByCartItemOrderByIngredientIdAsc(CartItem cartItem);
}
