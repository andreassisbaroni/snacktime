package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.model.SnackIngredient;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface to {@link SnackIngredient} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface SnackIngredientRepository extends JpaRepository<SnackIngredient, Long> {

    /**
     * Finds all {@link SnackIngredient} entities by it's your {@link Snack}.
     *
     * @param snack {@link Snack} entity.
     * @return a {@link Collection} of {@link SnackIngredient} entities.
     */
    Collection<SnackIngredient> findBySnack(Snack snack);
}
