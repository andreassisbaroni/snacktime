package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface to {@link Cart} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface CartRepository extends JpaRepository<Cart, Long> {

}
