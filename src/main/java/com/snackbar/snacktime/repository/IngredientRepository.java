package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface to {@link Ingredient} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

    /**
     * Finds a {@link Ingredient} by it's your description.
     *
     * @param description {@link String} representing a description of a
     * {@link Ingredient}.
     * @return a {@link Ingredient} entity.
     */
    Ingredient findByDescription(String description);
}
