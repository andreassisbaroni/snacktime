package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.Cart;
import com.snackbar.snacktime.domain.model.CartItem;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface to {@link CartItem} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface CartItemRepository extends JpaRepository<CartItem, Long> {

    /**
     * Finds a {@link Collection} of {@link CartItem} by it's your {@link Cart}.
     *
     * @param cart {@link Cart} entity.
     * @return a {@link Collection} of {@link CartItem} entities.
     */
    Collection<CartItem> findByCartOrderByIdAsc(Cart cart);
}
