package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.Snack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface to {@link Snack} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Repository
public interface SnackRepository extends JpaRepository<Snack, Long> {

}
