package com.snackbar.snacktime.controller;

import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.model.SnackIngredient;
import com.snackbar.snacktime.domain.service.SnackIngredientService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andre
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class SnackIngredientController extends BaseController {

    @Autowired
    private SnackIngredientService snackIngredientService;

    /**
     * Finds a {@link Collection} of {@link SnackIngredient} by a {@link Snack}
     * identifier.
     *
     * @param snackId {@link Snack} identifier.ordering by your id crescent.
     * @return a {@link Collection} of {@link SnackIngredient}.
     * @throws EntityNotExistsException indicates that @param snackId is not of
     * a valid {@link Snack}.
     */
    @RequestMapping(value = "/api/snacks/{snackId}/ingredients",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<SnackIngredient> findBySnackId(@PathVariable("snackId") Long snackId) throws EntityNotExistsException {
        return this.snackIngredientService.findBySnackId(snackId);
    }
}
