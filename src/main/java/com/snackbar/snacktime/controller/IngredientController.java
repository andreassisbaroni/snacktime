package com.snackbar.snacktime.controller;

import com.snackbar.snacktime.domain.model.Ingredient;
import com.snackbar.snacktime.domain.service.IngredientService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andre
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "api/ingredients")
public class IngredientController extends BaseController {

    @Autowired
    private IngredientService ingredientService;

    /**
     * Finds a {@link Ingredient} by its your identifier.
     *
     * @param id a {@link Ingredient} identifier.
     * @return a {@link Ingredient} entity.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     */
    @RequestMapping(value = "{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Ingredient findById(@PathVariable("id") Long id) throws EntityNotExistsException {
        return this.ingredientService.findById(id);
    }

    /**
     * Finds all {@link Ingredient} entities.
     *
     * @return a {@link Collection} with {@link Ingredient} entities.
     */
    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Ingredient> listAll() {
        return this.ingredientService.listAll();
    }
}
