package com.snackbar.snacktime.controller;

import com.snackbar.snacktime.domain.model.Cart;
import com.snackbar.snacktime.domain.model.CartItem;
import com.snackbar.snacktime.domain.model.CartItemIngredient;
import com.snackbar.snacktime.domain.service.CartItemService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andre
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/carts/items")
public class CartItemController extends BaseController {

    @Autowired
    private CartItemService cartItemService;

    /**
     * Removes a {@link CartItem} by your identifier, too removes all
     * {@link CartItemIngredient} entities.
     *
     * @param id a {@link CartItem} identifier.
     * @throws EntityNotExistsException indicates the @param id isn't valid.
     */
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteWithAllIngredients(@PathVariable("id") Long id) throws EntityNotExistsException {
        this.cartItemService.deleteWithAllIngredients(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Creates a {@link Cart}.
     *
     * @param id {@link CartItem} identifier.
     * @param cartItem a {@link CartItem} entity.
     * @return a {@link Cart} entity with your identifier.
     * @throws IdConflictException indicates that @param id isn't the same id of
     * param {@literal cartItem}.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     */
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CartItem updateAllIngredients(@PathVariable("id") Long id, @RequestBody CartItem cartItem) throws IdConflictException, EntityNotExistsException {
        CartItem updatedCartItem = cartItemService.updateAllIngredients(id, cartItem);

        return updatedCartItem;
    }

}
