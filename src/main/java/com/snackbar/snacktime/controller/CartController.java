package com.snackbar.snacktime.controller;

import com.snackbar.snacktime.domain.model.Cart;
import com.snackbar.snacktime.domain.model.CartItem;
import com.snackbar.snacktime.domain.model.CartItemIngredient;
import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.service.CartService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andre
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/carts")
public class CartController extends BaseController {

    @Autowired
    private CartService cartService;

    /**
     * Removes a {@link Cart}.
     *
     * @param id a {@link Cart} identifier.
     * @throws EntityNotExistsException indicates the @param id isn't valid.
     */
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) throws EntityNotExistsException {
        this.cartService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Add a new {@link Snack} to current cart, generating a new
     * {@link CartItem} idenfier, will too to create all importants
     * {@link CartItemIngredient} entities.
     *
     *
     * @param snack a {@link Snack} entity.
     * @return a {@link Cart} entity with your identifier.
     * @throws IdExistsException indicates that @param snackId is not of a valid
     * {@link Snack}.
     */
    @RequestMapping(
            value = "/",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Cart addSnackToCart(@RequestBody Snack snack) throws IdExistsException {
        Cart addedCart = cartService.addSnackToCart(snack);

        return addedCart;
    }

    /**
     * In this software, has just one {@link Cart}, and this method will find
     * it. This occurs because don't have user to make a order, then is
     * impossible vinculate a {@link Cart} with a session. Case is the first
     * session, this method will create a new {@link Cart} to work with it.
     *
     * @return a {@link Cart} entity.
     */
    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Cart findTheUnitCartThatSouldBeCreatedOrPersistOne() {
        return this.cartService.findTheUniqueCartThatSouldBeCreatedOrPersistOne();
    }
}
