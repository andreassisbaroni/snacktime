package com.snackbar.snacktime.controller;

import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.service.SnackService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andre
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/snacks")
public class SnackController extends BaseController {

    @Autowired
    private SnackService snackService;

    /**
     * Finds a {@link Snack} by its your identifier.
     *
     * @param id a {@link Snack} identifier.
     * @return a {@link Snack} entity.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     */
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Snack findById(@PathVariable("id") Long id) throws EntityNotExistsException {
        return this.snackService.findById(id);
    }

    /**
     * Finds all {@link Snack} entities.
     *
     * @return a {@link Collection} with {@link Snack} entities.
     */
    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Snack> listAll() {
        return this.snackService.listAll();
    }
}
