package com.snackbar.snacktime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;

@SpringBootApplication
public class SnacktimeApplication {

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("ValidationMessages", "i18n/messages");
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }

    public static void main(String[] args) {
        SpringApplication.run(SnacktimeApplication.class, args);
    }
}
