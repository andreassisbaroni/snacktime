package com.snackbar.snacktime.domain.service;

import com.snackbar.snacktime.domain.model.Cart;
import com.snackbar.snacktime.domain.model.CartItem;
import com.snackbar.snacktime.domain.model.CartItemIngredient;
import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdExistsException;

/**
 * Service class of {@link Cart} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface CartService {

    /**
     * Add a new {@link Snack} to current cart, generating a new
     * {@link CartItem} idenfier, will too to create all importants
     * {@link CartItemIngredient} entities.
     *
     *
     * @param snack a {@link Snack} entity.
     * @return a {@link Cart} entity with your identifier.
     * @throws IdExistsException indicates that @param snackId is not of a valid
     * {@link Snack}.
     */
    Cart addSnackToCart(Snack snack) throws IdExistsException;

    /**
     * Removes a {@link Cart}.
     *
     * @param id a {@link Cart} identifier.
     * @throws EntityNotExistsException indicates the @param id isn't valid.
     */
    void delete(Long id) throws EntityNotExistsException;

    /**
     * In this software, has just one {@link Cart}, and this method will find
     * it. This occurs because don't have user to make a order, then is
     * impossible vinculate a {@link Cart} with a session. Case is the first
     * session, this method will create a new {@link Cart} to work with it.
     *
     * @return a {@link Cart} entity.
     */
    Cart findTheUniqueCartThatSouldBeCreatedOrPersistOne();

    /**
     * Finds a {@link Cart} by its your identifier.
     *
     * @param id a {@link Cart} identifier.
     * @return a {@link Cart} entity.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     */
    Cart findById(Long id) throws EntityNotExistsException;
}
