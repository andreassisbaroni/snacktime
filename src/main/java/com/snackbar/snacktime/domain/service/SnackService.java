package com.snackbar.snacktime.domain.service;

import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdConflictException;
import com.snackbar.snacktime.exception.IdExistsException;
import java.util.Collection;

/**
 * Service class of {@link Snack} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface SnackService {

    /**
     * Creates a {@link Snack}.
     *
     * @param snack a {@link Snack} entity.
     * @return a {@link Snack} entity with your identifier.
     * @throws IdExistsException indicates that @param snack has a id.
     */
    Snack create(Snack snack) throws IdExistsException;

    /**
     * Updates a {@link Snack}.
     *
     * @param id a {@link Snack} identifier.
     * @param snack a {@link Snack} entity with changes.
     * @return the same {@link Snack} that was changed.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     * @throws IdConflictException indicates the @param id does not match with
     * the identifier containing in @param snack.
     */
    Snack update(Long id, Snack snack) throws EntityNotExistsException, IdConflictException;

    /**
     * Removes a {@link Snack}.
     *
     * @param id a {@link Snack} identifier.
     * @throws EntityNotExistsException indicates the @param id isn't valid.
     */
    void delete(Long id) throws EntityNotExistsException;

    /**
     * Finds all {@link Snack} entities.
     *
     * @return a {@link Collection} with {@link Snack} entities.
     */
    Collection<Snack> listAll();

    /**
     * Finds a {@link Snack} by its your identifier.
     *
     * @param id a {@link Snack} identifier.
     * @return a {@link Snack} entity.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     */
    Snack findById(Long id) throws EntityNotExistsException;
}
