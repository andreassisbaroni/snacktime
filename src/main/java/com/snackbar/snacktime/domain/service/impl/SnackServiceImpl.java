package com.snackbar.snacktime.domain.service.impl;

import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.service.SnackService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdConflictException;
import com.snackbar.snacktime.exception.IdExistsException;
import com.snackbar.snacktime.repository.SnackRepository;
import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author andre
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SnackServiceImpl implements SnackService {

    @Autowired
    private SnackRepository snackRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Snack create(Snack snack) throws IdExistsException {
        if (snack.getId() != null) {
            throw new IdExistsException(this.messageSource.getMessage("service.snack.idExists", null, null));
        }

        return this.snackRepository.save(snack);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Snack update(Long id, Snack snack) throws EntityNotExistsException, IdConflictException {
        Optional<Snack> localSnack = this.snackRepository.findById(id);

        if (!localSnack.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.snack.entityNotExists", null, null));
        }

        if (!localSnack.get().getId().equals(id)) {
            throw new IdConflictException(this.messageSource.getMessage("service.snack.idConflicts", null, null));
        }

        return this.snackRepository.save(snack);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(Long id) throws EntityNotExistsException {
        Optional<Snack> snack = this.snackRepository.findById(id);

        if (!snack.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.snack.entityNotExists", null, null));
        }

        this.snackRepository.delete(snack.get());
    }

    @Override
    public Collection<Snack> listAll() {
        return this.snackRepository.findAll();
    }

    @Override
    public Snack findById(Long id) throws EntityNotExistsException {
        Optional<Snack> snack = this.snackRepository.findById(id);

        if (!snack.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.snack.entityNotExists", null, null));
        }

        return snack.get();
    }
}
