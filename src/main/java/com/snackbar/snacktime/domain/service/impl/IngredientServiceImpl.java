package com.snackbar.snacktime.domain.service.impl;

import com.snackbar.snacktime.domain.model.Ingredient;
import com.snackbar.snacktime.domain.service.IngredientService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdConflictException;
import com.snackbar.snacktime.exception.IdExistsException;
import com.snackbar.snacktime.repository.IngredientRepository;
import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author andre
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class IngredientServiceImpl implements IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Ingredient create(Ingredient ingredient) throws IdExistsException {
        if (ingredient.getId() != null) {
            throw new IdExistsException(this.messageSource.getMessage("service.ingredient.idExists", null, null));
        }

        return this.ingredientRepository.save(ingredient);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Ingredient update(Long id, Ingredient ingredient) throws EntityNotExistsException, IdConflictException {
        Optional<Ingredient> localIngredient = this.ingredientRepository.findById(id);

        if (!localIngredient.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.ingredient.entityNotExists", null, null));
        }

        if (!localIngredient.get().getId().equals(id)) {
            throw new IdConflictException(this.messageSource.getMessage("service.ingredient.idConflicts", null, null));
        }

        return this.ingredientRepository.save(ingredient);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(Long id) throws EntityNotExistsException {
        Optional<Ingredient> ingredient = this.ingredientRepository.findById(id);

        if (!ingredient.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.ingredient.entityNotExists", null, null));
        }

        this.ingredientRepository.delete(ingredient.get());
    }

    @Override
    public Collection<Ingredient> listAll() {
        return this.ingredientRepository.findAll();
    }

    @Override
    public Ingredient findById(Long id) throws EntityNotExistsException {
        Optional<Ingredient> ingredient = this.ingredientRepository.findById(id);

        if (!ingredient.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.ingredient.entityNotExists", null, null));
        }

        return ingredient.get();
    }

}
