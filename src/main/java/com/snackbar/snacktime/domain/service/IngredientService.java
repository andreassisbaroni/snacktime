package com.snackbar.snacktime.domain.service;

import com.snackbar.snacktime.domain.model.Ingredient;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdConflictException;
import com.snackbar.snacktime.exception.IdExistsException;
import java.util.Collection;

/**
 * Service class of {@link Ingredient} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface IngredientService {

    /**
     * Creates a {@link Ingredient}.
     *
     * @param ingredient a {@link Ingredient} entity.
     * @return a {@link Ingredient} entity with your identifier.
     * @throws IdExistsException indicates that @param ingredient has a id.
     */
    Ingredient create(Ingredient ingredient) throws IdExistsException;

    /**
     * Updates a {@link Ingredient}.
     *
     * @param id a {@link Ingredient} identifier.
     * @param ingredient a {@link Ingredient} entity with changes.
     * @return the same {@link Ingredient} that was changed.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     * @throws IdConflictException indicates the @param id does not match with
     * the identifier containing in @param ingredient.
     */
    Ingredient update(Long id, Ingredient ingredient) throws EntityNotExistsException, IdConflictException;

    /**
     * Removes a {@link Ingredient}.
     *
     * @param id a {@link Ingredient} identifier.
     * @throws EntityNotExistsException indicates the @param id isn't valid.
     */
    void delete(Long id) throws EntityNotExistsException;

    /**
     * Finds all {@link Ingredient} entities.
     *
     * @return a {@link Collection} with {@link Ingredient} entities.
     */
    Collection<Ingredient> listAll();

    /**
     * Finds a {@link Ingredient} by its your identifier.
     *
     * @param id a {@link Ingredient} identifier.
     * @return a {@link Ingredient} entity.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     */
    Ingredient findById(Long id) throws EntityNotExistsException;
}
