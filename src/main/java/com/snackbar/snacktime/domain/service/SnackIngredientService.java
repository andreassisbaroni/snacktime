package com.snackbar.snacktime.domain.service;

import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.model.SnackIngredient;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import java.util.Collection;

/**
 * Service class of {@link SnackSnackIngredient} entity.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface SnackIngredientService {

    /**
     * Finds all {@link SnackIngredient} entities.
     *
     * @return a {@link Collection} of {@link SnackIngredient} entities.
     */
    Collection<SnackIngredient> listAll();

    /**
     * Finds a {@link Collection} of {@link SnackIngredient} by a {@link Snack}
     * identifier.
     *
     * @param snackId {@link Snack} identifier.ordering by your id crescent.
     * @return a {@link Collection} of {@link SnackIngredient}.
     * @throws EntityNotExistsException indicates that @param snackId is not of
     * a valid {@link Snack}.
     */
    Collection<SnackIngredient> findBySnackId(Long snackId) throws EntityNotExistsException;
}
