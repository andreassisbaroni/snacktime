package com.snackbar.snacktime.domain.service.impl;

import com.snackbar.snacktime.domain.model.Cart;
import com.snackbar.snacktime.domain.model.CartItem;
import com.snackbar.snacktime.domain.model.CartItemIngredient;
import com.snackbar.snacktime.domain.model.Ingredient;
import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.model.SnackIngredient;
import com.snackbar.snacktime.domain.model.pk.CartItemIngredientPK;
import com.snackbar.snacktime.domain.service.CartService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdExistsException;
import com.snackbar.snacktime.repository.CartItemIngredientRepository;
import com.snackbar.snacktime.repository.CartItemRepository;
import com.snackbar.snacktime.repository.CartRepository;
import com.snackbar.snacktime.repository.SnackIngredientRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author andre
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private SnackIngredientRepository snackIngredientRepository;

    @Autowired
    private CartItemIngredientRepository cartItemIngredientRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Cart addSnackToCart(Snack snack) throws IdExistsException {
        Cart cart = this.findTheUniqueCartThatSouldBeCreatedOrPersistOne();

        CartItem cartItem = new CartItem(snack, cart);

        cartItem.setPrice(snack.getPrice());
        cartItem = this.cartItemRepository.save(cartItem);

        if (cart.getCartItems() == null) {
            cart.setCartItems(new ArrayList<>());
        }
        cart.getCartItems().add(cartItem);

        this.generateCartItemIngredients(cartItem);

        return cart;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(Long id) throws EntityNotExistsException {
        Optional<Cart> cart = this.cartRepository.findById(id);

        if (!cart.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.cart.entityNotExists", null, null));
        }

        this.cartRepository.delete(cart.get());
    }

    @Override
    public Cart findTheUniqueCartThatSouldBeCreatedOrPersistOne() {
        Collection<Cart> carts = this.cartRepository.findAll();
        if (carts.isEmpty()) {
            carts.add(this.cartRepository.save(new Cart()));
        }

        Cart returnCart = carts.iterator().next();

        returnCart.setCartItems(this.cartItemRepository.findByCartOrderByIdAsc(returnCart));

        if (!returnCart.getCartItems().isEmpty()) {
            for (CartItem cartItem : returnCart.getCartItems()) {
                cartItem.setCartItemIngredients(this.cartItemIngredientRepository.findByCartItemOrderByIngredientIdAsc(cartItem));
            }
        }

        return returnCart;
    }

    @Override
    public Cart findById(Long id) throws EntityNotExistsException {
        Optional<Cart> cart = this.cartRepository.findById(id);

        if (!cart.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.cart.entityNotExists", null, null));
        }

        return cart.get();
    }

    /**
     * Generates the {@link CartItemIngredient} according with the fixed
     * {@link Product} {@link Ingredient}. This method will verify if the given
     * {@link CartItem} has {@link CartItemIngredient}.
     *
     * @param cartItem a {@link CartItem} entity without generated yours
     * ingredients.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    private void generateCartItemIngredients(CartItem cartItem) {
        if (this.cartItemIngredientRepository.findByCartItemOrderByIngredientIdAsc(cartItem).isEmpty()) {
            Collection<SnackIngredient> snackIngredients = this.snackIngredientRepository.findBySnack(cartItem.getSnack());
            for (SnackIngredient snackIngredient : snackIngredients) {
                CartItemIngredient cartItemIngredient = new CartItemIngredient(new CartItemIngredientPK(cartItem.getId(), snackIngredient.getIngredient().getId()),
                        snackIngredient.getAmount(),
                        cartItem,
                        snackIngredient.getIngredient());

                cartItemIngredient = this.cartItemIngredientRepository.save(cartItemIngredient);

                if (cartItem.getCartItemIngredients() == null) {
                    cartItem.setCartItemIngredients(new ArrayList<>());
                }

                cartItem.getCartItemIngredients().add(cartItemIngredient);
            }
        }
    }
}
