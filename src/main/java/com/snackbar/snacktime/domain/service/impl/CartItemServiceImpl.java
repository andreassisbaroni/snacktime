package com.snackbar.snacktime.domain.service.impl;

import com.snackbar.snacktime.domain.model.CartItem;
import com.snackbar.snacktime.domain.model.CartItemIngredient;
import com.snackbar.snacktime.domain.model.Ingredient;
import com.snackbar.snacktime.domain.service.CartItemService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdConflictException;
import com.snackbar.snacktime.repository.CartItemIngredientRepository;
import com.snackbar.snacktime.repository.CartItemRepository;
import com.snackbar.snacktime.repository.IngredientRepository;
import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author andre
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CartItemServiceImpl implements CartItemService {

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private CartItemIngredientRepository cartItemIngredientRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private MessageSource messages;

    @Override
    public CartItem updateAllIngredients(Long id, CartItem cartItem) throws IdConflictException, EntityNotExistsException {
        if (!id.equals(cartItem.getId())) {
            throw new IdConflictException(messages.getMessage("", null, null));
        }

        if (cartItem.getCartItemIngredients() != null && !cartItem.getCartItemIngredients().isEmpty()) {
            for (CartItemIngredient cartItemIngredient : cartItem.getCartItemIngredients()) {
                this.cartItemIngredientRepository.save(cartItemIngredient);
            }
        }

        this.updatePriceWithPromotions(id);

        return cartItem;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteWithAllIngredients(Long id) throws EntityNotExistsException {
        Optional<CartItem> cartItem = this.cartItemRepository.findById(id);

        if (!cartItem.isPresent()) {
            throw new EntityNotExistsException(messages.getMessage("service.cart.item.entityNotExists", null, null));
        }

        Collection<CartItemIngredient> cartItemIngredients = this.cartItemIngredientRepository.findByCartItemOrderByIngredientIdAsc(cartItem.get());

        for (CartItemIngredient cartItemIngredient : cartItemIngredients) {
            this.cartItemIngredientRepository.delete(cartItemIngredient);
        }

        this.cartItemRepository.delete(cartItem.get());
    }

    @Override
    public float calculatePriceWithPromotions(CartItem cartItem) {
        return this.calculatePriceWithoutPromotions(cartItem) - this.calculateTotalDiscounts(cartItem);
    }

    @Override
    public float calculatePriceWithoutPromotions(CartItem cartItem) {
        float totalPrice = 0;
        for (CartItemIngredient cartItemIngredient : cartItem.getCartItemIngredients()) {
            totalPrice = totalPrice + (cartItemIngredient.getAmount() * cartItemIngredient.getIngredient().getPrice());
        }

        return totalPrice;
    }

    /**
     * Calculates total discounts of a {@link CartItem}.
     *
     * @param cartItem {@link CartItem} entity.
     * @return the total of discounts.
     */
    private float calculateTotalDiscounts(CartItem cartItem) {
        float totalDiscount = 0;
        if (cartItem.getCartItemIngredients() != null && !cartItem.getCartItemIngredients().isEmpty()) {
            totalDiscount += this.calculateDiscountByPromotionLight(cartItem);
            totalDiscount += this.calculateDiscountByPromotionMuitaCarne(cartItem);
            totalDiscount += this.calculateDiscountByPromotionMuitoQueijo(cartItem);
        }
        return totalDiscount;
    }

    /**
     * Calcule the discount of promotion {@literal Light}.
     *
     * @param cartItem {@link CartItem} entity.
     * @return the value of discount of the promotion {@literal Light}.
     */
    private float calculateDiscountByPromotionLight(CartItem cartItem) {
        Ingredient lettuce = this.ingredientRepository.findByDescription("Alface");
        Ingredient bacon = this.ingredientRepository.findByDescription("Bacon");

        boolean hasLettuce = false;
        boolean hasBacon = false;
        for (CartItemIngredient cartItemIngredient : cartItem.getCartItemIngredients()) {
            if (!hasLettuce && cartItemIngredient.getIngredient().equals(lettuce) && cartItemIngredient.getAmount() > 0) {
                hasLettuce = true;
            }
            if (!hasBacon && cartItemIngredient.getIngredient().equals(bacon) && cartItemIngredient.getAmount() > 0) {
                hasBacon = true;
            }
        }

        if (hasLettuce && !hasBacon) {
            return (float) (this.calculatePriceWithoutPromotions(cartItem) * 0.1);
        }

        return 0f;
    }

    /**
     * Calcule the discount of promotion {@literal Muita Carne}.
     *
     * @param cartItem {@link CartItem} entity.
     * @return the value of discount of the promotion {@literal Muita Carne}.
     */
    private float calculateDiscountByPromotionMuitaCarne(CartItem cartItem) {
        Ingredient meat = this.ingredientRepository.findByDescription("Hamburguer de Carne");

        int demand = 0;
        for (CartItemIngredient cartItemIngredient : cartItem.getCartItemIngredients()) {
            if (cartItemIngredient.getIngredient().equals(meat)) {
                demand = (int) Math.floor(cartItemIngredient.getAmount() / 3);
            }
        }

        if (demand > 0) {
            return demand * meat.getPrice();
        }

        return 0f;
    }

    /**
     * Calcule the discount of promotion {@literal Muito Queijo}.
     *
     * @param cartItem {@link CartItem} entity.
     * @return the value of discount of the promotion {@literal Muito Queijo}.
     */
    private float calculateDiscountByPromotionMuitoQueijo(CartItem cartItem) {
        Ingredient cheese = this.ingredientRepository.findByDescription("Queijo");

        int demand = 0;
        for (CartItemIngredient cartItemIngredient : cartItem.getCartItemIngredients()) {
            if (cartItemIngredient.getIngredient().equals(cheese)) {
                demand = (int) Math.floor(cartItemIngredient.getAmount() / 3);
            }
        }

        if (demand > 0) {
            return demand * cheese.getPrice();
        }

        return 0f;
    }

    /**
     * Updates the price of a {@link CartItem} by your identifier.
     *
     * @param cartItemId {@link CartItem} identifier.
     * @throws EntityNotExistsException indicates that identifier does not match
     * with any {@link CartItem} entity in database.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    private void updatePriceWithPromotions(Long cartItemId) throws EntityNotExistsException {
        Optional<CartItem> cartItem = this.cartItemRepository.findById(cartItemId);

        if (!cartItem.isPresent()) {
            throw new EntityNotExistsException(messages.getMessage("service.cart.item.entityNotExists", null, null));
        }

        cartItem.get().setPrice(this.calculatePriceWithPromotions(cartItem.get()));

        this.cartItemRepository.save(cartItem.get());
    }

}
