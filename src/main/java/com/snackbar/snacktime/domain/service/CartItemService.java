package com.snackbar.snacktime.domain.service;

import com.snackbar.snacktime.domain.model.Cart;
import com.snackbar.snacktime.domain.model.CartItem;
import com.snackbar.snacktime.domain.model.CartItemIngredient;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.exception.IdConflictException;

/**
 * .
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
public interface CartItemService {

    /**
     * Creates a {@link Cart}.
     *
     * @param id {@link CartItem} identifier.
     * @param cartItem a {@link CartItem} entity.
     * @return a {@link Cart} entity with your identifier.
     * @throws IdConflictException indicates that @param id isn't the same id of
     * param {@literal cartItem}.
     * @throws EntityNotExistsException indicates that the @param id not match
     * with any entities in database.
     */
    CartItem updateAllIngredients(Long id, CartItem cartItem) throws IdConflictException, EntityNotExistsException;

    /**
     * Removes a {@link CartItem} by your identifier, too removes all
     * {@link CartItemIngredient} entities.
     *
     * @param id a {@link CartItem} identifier.
     * @throws EntityNotExistsException indicates the @param id isn't valid.
     */
    void deleteWithAllIngredients(Long id) throws EntityNotExistsException;

    /**
     * Calcules the price with all promotions of a given {@link CartItem} and
     * return the value.
     *
     * @param cartItem {@link CartItem} that will be calculated.
     * @return the price with promotions of the {@link CartItem}.
     */
    float calculatePriceWithPromotions(CartItem cartItem);

    /**
     * Calcules the price without all promotions of a given {@link CartItem} and
     * return the value.
     *
     * @param cartItem {@link CartItem} that will be calculated.
     * @return the price without promotions of the {@link CartItem}.
     */
    float calculatePriceWithoutPromotions(CartItem cartItem);
}
