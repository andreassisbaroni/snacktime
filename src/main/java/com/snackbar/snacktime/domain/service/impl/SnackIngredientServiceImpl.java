package com.snackbar.snacktime.domain.service.impl;

import com.snackbar.snacktime.domain.model.Snack;
import com.snackbar.snacktime.domain.model.SnackIngredient;
import com.snackbar.snacktime.domain.service.SnackIngredientService;
import com.snackbar.snacktime.exception.EntityNotExistsException;
import com.snackbar.snacktime.repository.SnackIngredientRepository;
import com.snackbar.snacktime.repository.SnackRepository;
import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author andre
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SnackIngredientServiceImpl implements SnackIngredientService {

    @Autowired
    private SnackIngredientRepository snackIngredientRepository;
    
    @Autowired
    private SnackRepository snackRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public Collection<SnackIngredient> listAll() {
        return this.snackIngredientRepository.findAll();
    }

    @Override
    public Collection<SnackIngredient> findBySnackId(Long snackId) throws EntityNotExistsException  {
        Optional<Snack> snack = this.snackRepository.findById(snackId);
        
        if (!snack.isPresent()) {
            throw new EntityNotExistsException(this.messageSource.getMessage("service.snack.ingredient.entityNotExists", null, null));
        }
        
        return this.snackIngredientRepository.findBySnack(snack.get());
    }

}
