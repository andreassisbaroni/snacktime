package com.snackbar.snacktime.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Class representing a shopping cart.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Entity
@Table(name = "cart")
public class Cart implements Serializable {

    @Id
    @SequenceGenerator(name = "cart_generator", sequenceName = "cart_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_generator")
    @JsonProperty("id")
    private Long id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "cart")
    @JsonProperty("cartItems")
    private Collection<CartItem> cartItems;

    public Cart() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Collection<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    @JsonProperty("price")
    public float getPrice() {
        float price = 0;
        if (this.cartItems != null) {
            for (CartItem cartItem : this.cartItems) {
                price = price + cartItem.getPrice();
            }
        }

        return price;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cart other = (Cart) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(this.getPrice());
    }
}
