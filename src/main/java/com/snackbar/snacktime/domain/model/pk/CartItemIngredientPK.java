package com.snackbar.snacktime.domain.model.pk;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Class representing a Snack.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Embeddable
public class CartItemIngredientPK implements Serializable {

    @NotNull
    @Column(name = "cart_item_id")
    private long cartItemId;

    @NotNull
    @Column(name = "ingredient_id")
    private long ingredientId;

    public CartItemIngredientPK() {
    }

    public CartItemIngredientPK(long cartItemId, long ingredientId) {
        this.cartItemId = cartItemId;
        this.ingredientId = ingredientId;
    }

    public long getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(long cartItemId) {
        this.cartItemId = cartItemId;
    }

    public long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (int) (this.cartItemId ^ (this.cartItemId >>> 32));
        hash = 79 * hash + (int) (this.ingredientId ^ (this.ingredientId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CartItemIngredientPK other = (CartItemIngredientPK) obj;
        if (this.cartItemId != other.cartItemId) {
            return false;
        }
        if (this.ingredientId != other.ingredientId) {
            return false;
        }
        return true;
    }
}
