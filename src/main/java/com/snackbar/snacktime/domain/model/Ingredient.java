package com.snackbar.snacktime.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Class representing a Ingredient.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Entity
@Table(name = "ingredient")
public class Ingredient implements Serializable {

    @Id
    @SequenceGenerator(name = "ingredient_generator", sequenceName = "ingredient_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ingredient_generator")
    @JsonProperty("id")
    private Long id;

    @Column(name = "description", nullable = false)
    @JsonProperty("description")
    private String description;

    @Column(name = "price", nullable = false, precision = 12, scale = 2)
    @JsonProperty("price")
    private float price;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ingredient")
    @JsonIgnore
    private List<SnackIngredient> snackIngredients;

    public Ingredient() {
    }

    public Ingredient(String description, float price) {
        this.description = description;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<SnackIngredient> getSnackIngredients() {
        return snackIngredients;
    }

    public void setSnackIngredients(List<SnackIngredient> snackIngredients) {
        this.snackIngredients = snackIngredients;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ingredient other = (Ingredient) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.description == null ? "" : this.description;
    }
}
