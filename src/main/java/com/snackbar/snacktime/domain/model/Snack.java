package com.snackbar.snacktime.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Class representing a Snack.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Entity
@Table(name = "snack")
public class Snack implements Serializable {

    @Id
    @SequenceGenerator(name = "snack_generator", sequenceName = "snack_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "snack_generator")
    @JsonProperty("id")
    private Long id;

    @Column(name = "description", nullable = false)
    @JsonProperty("description")
    private String description;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "snack")
    @JsonProperty("snackIngredients")
    private List<SnackIngredient> snackIngredients;

    public Snack() {
    }

    public Snack(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SnackIngredient> getSnackIngredients() {
        return snackIngredients;
    }

    public void setSnackIngredients(List<SnackIngredient> snackIngredients) {
        this.snackIngredients = snackIngredients;
    }

    @JsonProperty("price")
    public float getPrice() {
        float price = 0;
        if (this.snackIngredients != null) {
            for (SnackIngredient snackIngredient : this.snackIngredients) {
                price = price + snackIngredient.getPrice();
            }
        }

        return price;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj
    ) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Snack other = (Snack) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.description == null ? "" : this.description;
    }
}
