package com.snackbar.snacktime.domain.model.pk;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Class representing a Snack.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Embeddable
public class SnackIngredientPK implements Serializable {

    @NotNull
    @Column(name = "snack_id")
    private long snackId;

    @NotNull
    @Column(name = "ingredient_id")
    private long ingredientId;

    public SnackIngredientPK() {
    }

    public SnackIngredientPK(long snackId, long ingredientId) {
        this.snackId = snackId;
        this.ingredientId = ingredientId;
    }

    public long getSnackId() {
        return snackId;
    }

    public void setSnackId(long snackId) {
        this.snackId = snackId;
    }

    public long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (int) (this.snackId ^ (this.snackId >>> 32));
        hash = 79 * hash + (int) (this.ingredientId ^ (this.ingredientId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SnackIngredientPK other = (SnackIngredientPK) obj;
        if (this.snackId != other.snackId) {
            return false;
        }
        if (this.ingredientId != other.ingredientId) {
            return false;
        }
        return true;
    }
}
