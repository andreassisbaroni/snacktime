package com.snackbar.snacktime.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Class representing a item of shopping cart.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Entity
@Table(name = "cart_item")
public class CartItem implements Serializable {

    @Id
    @SequenceGenerator(name = "cart_item_generator", sequenceName = "cart_item_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_item_generator")
    @JsonProperty("id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "snack_id", referencedColumnName = "id")
    @JsonProperty("snack")
    private Snack snack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    @JsonIgnore
    private Cart cart;

    @OneToMany(mappedBy = "cartItem", fetch = FetchType.LAZY)
    @JsonProperty("cartItemIngredients")
    private Collection<CartItemIngredient> cartItemIngredients;

    @Column(name = "price")
    @JsonProperty("price")
    private float price;

    public CartItem() {
    }

    public CartItem(Snack snack, Cart cart) {
        this.snack = snack;
        this.cart = cart;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Snack getSnack() {
        return snack;
    }

    public void setSnack(Snack snack) {
        this.snack = snack;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Collection<CartItemIngredient> getCartItemIngredients() {
        return this.cartItemIngredients;
    }

    public void setCartItemIngredients(Collection<CartItemIngredient> cartItemIngredients) {
        this.cartItemIngredients = cartItemIngredients;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CartItem other = (CartItem) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(this.getPrice());
    }
}
