package com.snackbar.snacktime.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.snackbar.snacktime.domain.model.pk.CartItemIngredientPK;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class representing a ingredient of a item of shopping cart.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Entity
@Table(name = "cart_item_ingredient")
public class CartItemIngredient implements Serializable {

    @EmbeddedId
    private CartItemIngredientPK cartItemIngredientPK;

    @Column(name = "amount", nullable = false)
    @JsonProperty("amount")
    private int amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_item_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private CartItem cartItem;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ingredient_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonProperty("ingredient")
    private Ingredient ingredient;

    public CartItemIngredient() {
    }

    public CartItemIngredient(CartItemIngredientPK cartItemIngredientPK, int amount, CartItem cartItem, Ingredient ingredient) {
        this.cartItemIngredientPK = cartItemIngredientPK;
        this.amount = amount;
        this.cartItem = cartItem;
        this.ingredient = ingredient;
    }

    public CartItemIngredientPK getCartItemIngredientPK() {
        return cartItemIngredientPK;
    }

    public void setCartItemIngredientPK(CartItemIngredientPK cartItemIngredientPK) {
        this.cartItemIngredientPK = cartItemIngredientPK;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public CartItem getCartItem() {
        return cartItem;
    }

    public void setCartItem(CartItem cartItem) {
        this.cartItem = cartItem;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    @JsonProperty("price")
    public float getPrice() {
        float price = 0;
        if (this.getIngredient() != null && this.amount > 0) {
            price = this.getIngredient().getPrice() * this.amount;
        }

        return price;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.cartItemIngredientPK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CartItemIngredient other = (CartItemIngredient) obj;
        if (!Objects.equals(this.cartItemIngredientPK, other.cartItemIngredientPK)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(this.amount);
    }
}
