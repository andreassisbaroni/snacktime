package com.snackbar.snacktime.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snackbar.snacktime.domain.model.pk.SnackIngredientPK;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class representing a Ingredient of Snack.
 *
 * @author andre.baroni
 * @since 17/07/2018
 * @version 1.0
 */
@Entity
@Table(name = "snack_ingredient")
public class SnackIngredient implements Serializable {

    @EmbeddedId
    private SnackIngredientPK ingredientPK;

    @Column(name = "amount", nullable = false)
    @JsonProperty("amount")
    private int amount;

    @JoinColumn(name = "snack_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Snack snack;

    @JoinColumn(name = "ingredient_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JsonProperty("ingredient")
    private Ingredient ingredient;

    public SnackIngredient() {
    }

    public SnackIngredient(SnackIngredientPK ingredientPK) {
        this.ingredientPK = ingredientPK;
    }

    public SnackIngredientPK getIngredientPK() {
        return ingredientPK;
    }

    public void setIngredientPK(SnackIngredientPK ingredientPK) {
        this.ingredientPK = ingredientPK;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Snack getSnack() {
        return snack;
    }

    public void setSnack(Snack snack) {
        this.snack = snack;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    @JsonProperty("price")
    public float getPrice() {
        float totalPrice = 0;
        if (this.ingredient != null) {
            totalPrice = this.ingredient.getPrice() * this.amount;
        }

        return totalPrice;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.ingredientPK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SnackIngredient other = (SnackIngredient) obj;
        if (!Objects.equals(this.ingredientPK, other.ingredientPK)) {
            return false;
        }
        return true;
    }

}
