package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.Ingredient;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author andre.baroni
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class IngredientRepositoryTest {

    @Autowired
    private IngredientRepository ingredientRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldCreateAIngredient() {
        Ingredient ingredient = this.ingredientRepository.save(new Ingredient("Alface", 0.40f));

        Assertions.assertThat(ingredient.getId()).isNotNull();
        Assertions.assertThat(ingredient.getDescription()).isEqualTo("Alface");
        Assertions.assertThat(ingredient.getPrice()).isEqualTo(0.40f);
    }

    @Test
    public void shouldUpdateAIngredient() {
        Ingredient ingredient = this.ingredientRepository.save(new Ingredient("Alface", 0.40f));

        Assertions.assertThat(ingredient.getId()).isNotNull();
        Assertions.assertThat(ingredient.getDescription()).isEqualTo("Alface");
        Assertions.assertThat(ingredient.getPrice()).isEqualTo(0.40f);

        ingredient.setDescription("Bacon");
        ingredient.setPrice(2f);
        ingredient = this.ingredientRepository.save(ingredient);

        Assertions.assertThat(ingredient.getId()).isNotNull();
        Assertions.assertThat(ingredient.getDescription()).isEqualTo("Bacon");
        Assertions.assertThat(ingredient.getPrice()).isEqualTo(2f);
    }

    @Test
    public void shouldRemoveAIngredient() {
        Ingredient ingredient = this.ingredientRepository.save(new Ingredient("Alface", 0.40f));

        Assertions.assertThat(ingredient.getId()).isNotNull();
        Assertions.assertThat(ingredient.getDescription()).isEqualTo("Alface");
        Assertions.assertThat(ingredient.getPrice()).isEqualTo(0.40f);

        this.ingredientRepository.delete(ingredient);

        Assertions.assertThat(this.ingredientRepository.findById(ingredient.getId()).isPresent()).isFalse();
    }

    @Test
    public void shouldFindIngredientById() {
        Ingredient ingredient = this.ingredientRepository.save(new Ingredient("Alface", 0.40f));

        Assertions.assertThat(ingredient.getId()).isNotNull();
        Assertions.assertThat(ingredient.getDescription()).isEqualTo("Alface");
        Assertions.assertThat(ingredient.getPrice()).isEqualTo(0.40f);

        Optional<Ingredient> createdIngredient = this.ingredientRepository.findById(ingredient.getId());
        Assertions.assertThat(createdIngredient.isPresent()).isTrue();
        Assertions.assertThat(createdIngredient.get().getId()).isEqualTo(ingredient.getId());
        Assertions.assertThat(createdIngredient.get().getDescription()).isEqualTo(ingredient.getDescription());
        Assertions.assertThat(createdIngredient.get().getPrice()).isEqualTo(ingredient.getPrice());
    }
}
