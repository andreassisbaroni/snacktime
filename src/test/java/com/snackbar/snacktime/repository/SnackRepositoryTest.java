package com.snackbar.snacktime.repository;

import com.snackbar.snacktime.domain.model.Snack;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author andre.baroni
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SnackRepositoryTest {

    @Autowired
    private SnackRepository snackRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldCreateASnack() {
        Snack snack = this.snackRepository.save(new Snack("X-Bacon"));

        Assertions.assertThat(snack.getId()).isNotNull();
        Assertions.assertThat(snack.getDescription()).isEqualTo("X-Bacon");
    }

    @Test
    public void shouldUpdateASnack() {
        Snack snack = this.snackRepository.save(new Snack("X-Bacon"));

        Assertions.assertThat(snack.getId()).isNotNull();
        Assertions.assertThat(snack.getDescription()).isEqualTo("X-Bacon");

        snack.setDescription("X-Salada");
        snack = this.snackRepository.save(snack);

        Assertions.assertThat(snack.getId()).isNotNull();
        Assertions.assertThat(snack.getDescription()).isEqualTo("X-Salada");
    }

    @Test
    public void shouldRemoveASnack() {
        Snack snack = this.snackRepository.save(new Snack("X-Bacon"));

        Assertions.assertThat(snack.getId()).isNotNull();
        Assertions.assertThat(snack.getDescription()).isEqualTo("X-Bacon");

        this.snackRepository.delete(snack);

        Assertions.assertThat(this.snackRepository.findById(snack.getId()).isPresent()).isFalse();
    }

    @Test
    public void shouldFindSnackById() {
        Snack snack = this.snackRepository.save(new Snack("X-Bacon"));

        Assertions.assertThat(snack.getId()).isNotNull();
        Assertions.assertThat(snack.getDescription()).isEqualTo("X-Bacon");

        Optional<Snack> createdSnack = this.snackRepository.findById(snack.getId());
        Assertions.assertThat(createdSnack.isPresent()).isTrue();
        Assertions.assertThat(createdSnack.get().getId()).isEqualTo(snack.getId());
        Assertions.assertThat(createdSnack.get().getDescription()).isEqualTo(snack.getDescription());
    }
}
